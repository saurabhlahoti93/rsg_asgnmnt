package com.assignment.interfaces;

/**
 * Created by Saurabh on 02-08-2016.
 */
public interface ListItemClickListener {
    void itemClicked(int position, Object obj, Object holder);
}
