package com.assignment.interfaces;

import com.assignment.entities.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by cl-macmini-80 on 8/2/16.
 */
public interface WebServices {

    @GET("task.txt")
    Call<List<Product>> getProducts();
}
