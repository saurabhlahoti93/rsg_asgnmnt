package com.assignment.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.assignment.R;
import com.assignment.adapters.AdapterProduct;
import com.assignment.entities.Product;
import com.assignment.entities.ProductWrapperClass;
import com.assignment.fragments.ProductDetailFragment;
import com.assignment.interfaces.ListItemClickListener;
import com.assignment.util.Constants;
import com.assignment.util.HidingScrollListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductActivity extends BaseActivity implements ListItemClickListener,
        OnClickListener {

    RecyclerView rvProducts;
    AdapterProduct adapterProduct;
    List<Product> products = new ArrayList<>();
    List<Product>original = new ArrayList<>();
    ProductWrapperClass productWrapperClass;
    CardView bottomLayout;
    SwipeRefreshLayout swipeRefreshLayout;
    AppBarLayout appBarLayout;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    ProductDetailFragment productDetailFragment;
    SORT_MODE sortMode = SORT_MODE.NONE;
    ImageView imgSortName, imgSortPrice, imgSortRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        initToolbar(getString(R.string.title_product));
        fragmentManager = getSupportFragmentManager();
        productDetailFragment = new ProductDetailFragment();
        rvProducts = (RecyclerView) findViewById(R.id.rvProducts);
        appBarLayout = (AppBarLayout) findViewById(R.id.toolbarLayout);
        imgSortName = (ImageView) findViewById(R.id.imgSortName);
        imgSortPrice = (ImageView) findViewById(R.id.imgSortPrice);
        imgSortRate = (ImageView) findViewById(R.id.imgSortRate);
        rvProducts.setLayoutManager(new GridLayoutManager(this, 2));
        adapterProduct = new AdapterProduct(this, products, this);
        rvProducts.setAdapter(adapterProduct);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getProducts();
            }
        });
        bottomLayout = (CardView) findViewById(R.id.bottomLayout);
        rvProducts.setOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });
        productWrapperClass = gson.fromJson(commonUtil.getStringFromSharedPreferences(KEY_PRODUCTS, this), ProductWrapperClass.class);
        if (productWrapperClass == null) {
            swipeRefreshLayout.setRefreshing(true);
        } else {
            original.addAll(productWrapperClass.getProducts());
            adapterProduct.setProducts(productWrapperClass.getProducts());
            adapterProduct.notifyDataSetChanged();
        }

        getProducts();

    }

    @Override
    public void onBackPressed() {
        fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = fragmentManager.findFragmentByTag(FRAGMENT_TAG);
        if (fragment == null) {
            super.onBackPressed();
        } else {
            showHomeLayout();
            fragmentTransaction.remove(fragment).commit();
        }
    }

    private void getProducts() {
        Call<List<Product>> call = webServices.getProducts();
        call.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.errorBody() != null) {
                    commonUtil.handleRetrofitError(context, response.code(), response.errorBody(), null);
                } else {
                    productWrapperClass = new ProductWrapperClass(response.body());
                    commonUtil.setStringInSharedPreferences(Constants.KEY_PRODUCTS, gson.toJson(productWrapperClass), context);
                    products = productWrapperClass.getProducts();
                    original.clear();
                    original.addAll(products);
                    adapterProduct.setProducts(products);
                    sortList(sortMode);
                    adapterProduct.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                commonUtil.handleRetrofitError(context, null, null, t);
            }
        });
    }

    @Override
    public void itemClicked(int position, Object obj, Object holder) {
        Bundle bundle = new Bundle();
        String imgTransitionName = "";
        AdapterProduct.MyHolder myHolder = (AdapterProduct.MyHolder) holder;


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imgTransitionName = myHolder.imgProduct.getTransitionName();
  /*          setSharedElementReturnTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(R.transition.change_image_trans));
            setExitTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(android.R.transition.fade));*/

            productDetailFragment.setSharedElementEnterTransition(TransitionInflater.from(
                    this).inflateTransition(R.transition.image_transition));
            productDetailFragment.setSharedElementReturnTransition(TransitionInflater.from(
                    this).inflateTransition(R.transition.image_transition));
            productDetailFragment.setEnterTransition(TransitionInflater.from(
                    this).inflateTransition(android.R.transition.fade));
            productDetailFragment.setExitTransition(TransitionInflater.from(
                    this).inflateTransition(android.R.transition.fade));
        }
        bundle.putString(TRANSITION_TAG, imgTransitionName);
        productDetailFragment.setProduct((Product) obj);
        productDetailFragment.setArguments(bundle);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, productDetailFragment, FRAGMENT_TAG);
        fragmentTransaction.addSharedElement(myHolder.imgProduct, imgTransitionName);
        fragmentTransaction.commit();
        hideHomeLayout((Product) obj);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.msg_logout));
                builder.setPositiveButton(getString(R.string.app_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        commonUtil.clearSharedPreferences();
                        startActivity(new Intent(context, LoginActivity.class));
                        finish();
                    }
                });
                builder.setNegativeButton(getString(R.string.app_no), null);
                builder.create().show();
                break;

            case R.id.layoutSortName:
                if(sortMode == SORT_MODE.NAME_ASC){
                    sortList(SORT_MODE.NAME_DSC);
                }else if(sortMode == SORT_MODE.NAME_DSC){
                    sortList(SORT_MODE.NONE);
                }else{
                    sortList(SORT_MODE.NAME_ASC);
                }
                break;

            case R.id.layoutSortRate:
                if(sortMode == SORT_MODE.RATE_ASC){
                    sortList(SORT_MODE.RATE_DSC);
                }else if(sortMode == SORT_MODE.RATE_DSC){
                    sortList(SORT_MODE.NONE);
                }else{
                    sortList(SORT_MODE.RATE_ASC);
                }
                break;

            case R.id.layoutSortPrice:
                if(sortMode == SORT_MODE.PRICE_ASC){
                    sortList(SORT_MODE.PRICE_DCS);
                }else if(sortMode == SORT_MODE.PRICE_DCS){
                    sortList(SORT_MODE.NONE);
                }else{
                    sortList(SORT_MODE.PRICE_ASC);
                }
                break;
        }
    }

    private void hideViews() {
        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) bottomLayout.getLayoutParams();
        int fabBottomMargin = lp.bottomMargin;
        bottomLayout.animate().translationY(bottomLayout.getHeight() + fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
    }

    private void showViews() {
        bottomLayout.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
    }

    void hideHomeLayout(Product product) {
        setTitle(product.getName());
        bottomLayout.setVisibility(View.GONE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appBarLayout.setExpanded(true);
//        swipeRefreshLayout.setVisibility(View.GONE);
    }

    void showHomeLayout() {
        setTitle(getString(R.string.title_product));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        bottomLayout.setVisibility(View.VISIBLE);
//        swipeRefreshLayout.setVisibility(View.VISIBLE);
    }

    void sortList(SORT_MODE sortMode) {
        this.sortMode = sortMode;
        initSort();
        adapterProduct.setProducts(products);
        switch (sortMode) {
            case NAME_ASC:
                commonUtil.sortNameAsc(products);
                imgSortName.setImageResource(R.drawable.icon_asc_highlight);
                break;

            case NAME_DSC:
                commonUtil.sortNamDsc(products);
                imgSortName.setImageResource(R.drawable.icon_dsc_highlight);
                break;

            case PRICE_ASC:
                commonUtil.sortPriceAsc(products);
                imgSortPrice.setImageResource(R.drawable.icon_asc_highlight);
                break;

            case PRICE_DCS:
                commonUtil.sortPriceDsc(products);
                imgSortPrice.setImageResource(R.drawable.icon_dsc_highlight);
                break;

            case RATE_ASC:
                commonUtil.sortRateAsc(products);
                imgSortRate.setImageResource(R.drawable.icon_asc_highlight);
                break;

            case RATE_DSC:
                commonUtil.sortRateDsc(products);
                imgSortRate.setImageResource(R.drawable.icon_dsc_highlight);
                break;

            case NONE:
                adapterProduct.setProducts(original);
                break;
        }
        adapterProduct.notifyDataSetChanged();
    }

    private void initSort() {
        imgSortPrice.setImageResource(R.drawable.icon_asc_sort);
        imgSortRate.setImageResource(R.drawable.icon_asc_sort);
        imgSortName.setImageResource(R.drawable.icon_asc_sort);
    }
}
