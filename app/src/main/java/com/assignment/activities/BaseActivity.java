package com.assignment.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.assignment.util.CommonUtil;

import com.assignment.GlobalClass;
import com.assignment.R;
import com.assignment.interfaces.WebServices;
import com.assignment.util.Constants;
import com.assignment.util.CustomProgress;
import com.google.gson.Gson;

/**
 * Created by Saurabh on 02-08-2016.
 */
public abstract class BaseActivity extends AppCompatActivity implements Constants {

    CommonUtil commonUtil;
    Context context;
    CustomProgress customProgress;
    Gson gson;
    WebServices webServices;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        commonUtil = new CommonUtil();
        customProgress = new CustomProgress(this, "Please wait ...", true);
        gson = new Gson();
        webServices= ((GlobalClass)getApplicationContext()).getInterface();

    }

    @Override
    protected void onResume() {
        super.onResume();
        CommonUtil.context = this;
        context = this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();
        CommonUtil.context = null;
        context = null;
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.activity_left_in, R.anim.activity_left_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_right_in, R.anim.activity_right_out);
    }

    public void initToolbar(String title) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
     /*   getSupportActionBar().setHomeAsUpIndicator(R.drawable.btn_back);*/
    }

    public void setTitle(String title){
        toolbar.setTitle(title);
    }
}
