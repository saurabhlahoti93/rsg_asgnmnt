package com.assignment.activities;

import android.content.Intent;
import android.os.Bundle;


import com.assignment.entities.FacebookDetail;
import com.assignment.R;
import com.assignment.util.Constants;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends BaseActivity implements Constants {
    CallbackManager callbackManager;
    AccessToken accessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(getApplication());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);

        FacebookDetail facebookDetail = gson.fromJson(commonUtil.getStringFromSharedPreferences(KEY_FACEBOOK, this),
                FacebookDetail.class);
        if (facebookDetail != null) {
            startActivity(new Intent(this, ProductActivity.class));
            finish();
        }else{
            try{
                LoginManager.getInstance().logOut();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        LoginButton loginButton = (LoginButton) findViewById(R.id.btnFb);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                accessToken = loginResult.getAccessToken();
                customProgress.showProgress();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                customProgress.dismissProgress();
                                try {
                                    FacebookDetail facebookDetail = new FacebookDetail(
                                            object.getString("id"),
                                            object.getString("gender"),
                                            object.getString("first_name"),
                                            object.getString("last_name"));
                                    commonUtil.setStringInSharedPreferences(KEY_FACEBOOK,
                                            gson.toJson(facebookDetail), LoginActivity.this);
                                    startActivity(new Intent(LoginActivity.this, ProductActivity.class ));
                                    finish();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "email,name,gender,first_name,last_name,picture");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                customProgress.dismissProgress();
            }

            @Override
            public void onError(FacebookException error) {
                customProgress.dismissProgress();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
