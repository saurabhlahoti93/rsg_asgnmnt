package com.assignment.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by cl-macmini-80 on 04/07/16.
 */
public class FacebookDetail implements Serializable{
    public String id;
    public String gender;
    @SerializedName("first_name")
    public String firstName;

    @SerializedName("last_name")
    public String lastName;

    public FacebookDetail(String id, String gender, String firstName, String lastName) {
        this.id = id;
        this.gender = gender;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
