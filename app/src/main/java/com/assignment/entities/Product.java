package com.assignment.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Saurabh on 02-08-2016.
 */
public class Product {
    int id;
    String name;
    String description;
    Float score;
    Integer quantity;
    String location;

    @SerializedName("min_rental_period")
    Integer minRentalPeriod;
    String tnc;

    @SerializedName("per_day_rent")
    Float perDayRent;

    @SerializedName("per_week_rent")
    Float perWeekRent;

    @SerializedName("per_month_rent")
    Float perMonthRent;

    List<Picture> pictures;
    List<String> blockedDates;
    @SerializedName("user_image_url")
    String userImageUrl;
    @SerializedName("product_locality")
    String productLocality;
    @SerializedName("offer_price")
    Float offerPrice;
    @SerializedName("offer_percentage")
    Float offerPercentage;
    Address address;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Float getScore() {
        return score;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public String getLocation() {
        return location;
    }

    public Integer getMinRentalPeriod() {
        return minRentalPeriod;
    }

    public String getTnc() {
        return tnc;
    }

    public Float getPerDayRent() {
        return perDayRent;
    }

    public Float getPerWeekRent() {
        return perWeekRent;
    }

    public Float getPerMonthRent() {
        return perMonthRent;
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public List<String> getBlockedDates() {
        return blockedDates;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public String getProductLocality() {
        return productLocality;
    }

    public Float getOfferPrice() {
        return offerPrice;
    }

    public Float getOfferPercentage() {
        return offerPercentage;
    }

    public Address getAddress() {
        return address;
    }

    public class Picture {
        String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public class Address {
        String name;
        String mobile;
        String address;
        String pincode;

        public String getName() {
            return name;
        }


        public String getMobile() {
            return mobile;
        }


        public String getAddress() {
            return address;
        }


        public String getPincode() {
            return pincode;
        }

    }
}
