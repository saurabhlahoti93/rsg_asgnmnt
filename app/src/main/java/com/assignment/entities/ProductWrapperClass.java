package com.assignment.entities;

import com.assignment.entities.Product;

import java.util.List;

/**
 * Created by Saurabh on 02-08-2016.
 */
public class ProductWrapperClass {
    List<Product>products;

    public ProductWrapperClass(List<Product> products) {
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
