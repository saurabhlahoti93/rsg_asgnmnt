package com.assignment.fragments;


import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.assignment.R;
import com.assignment.entities.Product;
import com.assignment.util.CommonUtil;
import com.assignment.util.Constants;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDetailFragment extends Fragment implements Constants,
        OnClickListener {

    View view;
    TextView tvRating, tvDesc, tvOfferPct, tvOfferPrice, tvRentDay, tvRentWeek,
            tvRentMonth, tvQuantity, tvPeriod, tvOwnerName, tvPhone, tvAdd, tvPincode, tvTNC;
    ImageView imgProduct, imgOwner, iconCal;
    CompactCalendarView compactCalendarView;
    Product product;
    CommonUtil commonUtil;
    LinearLayout layoutOffer;
    int colorBlocked;

    public ProductDetailFragment() {
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String transitionName = "";
        Bundle bundle = getArguments();
        commonUtil = new CommonUtil();
        if (bundle != null) {
            transitionName = bundle.getString(TRANSITION_TAG);
        }


        view = inflater.inflate(R.layout.fragment_product_detail, container, false);
        imgProduct = (ImageView) view.findViewById(R.id.imageView);
        imgOwner = (ImageView) view.findViewById(R.id.imgOwner);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imgProduct.setTransitionName(transitionName);
        }


        tvRating = (TextView) view.findViewById(R.id.tvRating);
        tvDesc = (TextView) view.findViewById(R.id.tvDescription);
        tvOfferPct = (TextView) view.findViewById(R.id.tvDiscount);
        tvOfferPrice = (TextView) view.findViewById(R.id.tvOfferPrice);
        tvRentDay = (TextView) view.findViewById(R.id.tvRentDay);
        tvRentWeek = (TextView) view.findViewById(R.id.tvRentWeek);
        tvRentMonth = (TextView) view.findViewById(R.id.tvRentMonth);
        tvQuantity = (TextView) view.findViewById(R.id.tvQuantity);
        tvPeriod = (TextView) view.findViewById(R.id.tvPeriod);
        tvOwnerName = (TextView) view.findViewById(R.id.tvOwnerName);
        tvPhone = (TextView) view.findViewById(R.id.tvMobile);
        tvAdd = (TextView) view.findViewById(R.id.tvAddress);
        tvPincode = (TextView) view.findViewById(R.id.tvPincode);
        tvTNC = (TextView) view.findViewById(R.id.tvTNC);
        layoutOffer = (LinearLayout) view.findViewById(R.id.layoutOffer);
        iconCal = (ImageView) view.findViewById(R.id.iconCal);
        compactCalendarView = (CompactCalendarView) view.findViewById(R.id.compactCalView);
        iconCal.setOnClickListener(this);

        init(product);
        return view;
    }

    private void init(Product product) {
        colorBlocked = ContextCompat.getColor(getActivity(), R.color.discount_color);
        tvRating.setText(product.getScore().toString());
        tvDesc.setText(product.getDescription());
        if (product.getBlockedDates().size() == 0) {
            iconCal.setVisibility(View.INVISIBLE);
        } else {
            iconCal.setVisibility(View.VISIBLE);
        }
        if (product.getOfferPercentage() == null || product.getOfferPercentage() == 0) {
            tvOfferPct.setText("Not Avail.");
            layoutOffer.setVisibility(View.GONE);
            tvOfferPct.setVisibility(View.GONE);
        } else {
            tvRentDay.setPaintFlags(tvRentDay.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tvOfferPct.setText(product.getOfferPercentage().intValue() + "% off");
        }
        if (product.getOfferPrice() == null || product.getOfferPrice() == 0) {
            tvOfferPrice.setText("Not Avail.");
        } else {
            tvOfferPrice.setText(getString(R.string.string_rupee) + product.getOfferPrice().toString());
        }
        if (product.getPerDayRent() == null || product.getPerDayRent() == 0) {
            tvRentDay.setText("Not Avail.");
        } else {
            tvRentDay.setText(getString(R.string.string_rupee) + product.getPerDayRent().toString());
        }
        if (product.getPerWeekRent() == null || product.getPerWeekRent() == 0) {
            tvRentWeek.setText("Not Avail.");
        } else {
            tvRentWeek.setText(getString(R.string.string_rupee) + product.getPerWeekRent().toString());
        }
        if (product.getPerMonthRent() == null || product.getPerMonthRent() == 0) {
            tvRentMonth.setText("Not Avail.");
        } else {
            tvRentMonth.setText(getString(R.string.string_rupee) + product.getPerMonthRent().toString());
        }
        if (product.getQuantity() == null || product.getQuantity() == 0) {
            tvQuantity.setText("Not Avail.");
        } else {
            tvQuantity.setText(product.getQuantity().toString());
        }
        if (product.getMinRentalPeriod() == null || product.getMinRentalPeriod() == 0) {
            tvPeriod.setText("Not Avail.");
        } else {
            tvPeriod.setText(product.getMinRentalPeriod().toString() + " month(s)");
        }
        tvOwnerName.setText(product.getAddress().getName());
        tvAdd.setText(product.getAddress().getAddress());
        tvPincode.setText(product.getAddress().getPincode());
        tvPhone.setText(product.getAddress().getMobile());

        String[] tncArr = product.getTnc().split("#");
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : tncArr) {
            stringBuilder.append(s + "\n");
        }
        tvTNC.setText(stringBuilder.toString());

        Picasso.with(getActivity())
                .load(product.getPictures().get(0).getUrl())
                .placeholder(R.drawable.placeholder)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(imgProduct);

        Picasso.with(getActivity())
                .load(product.getUserImageUrl())
                .placeholder(R.drawable.user_placeholder)
                .into(imgOwner);

        for (String strDate : product.getBlockedDates()) {
            Event ev1 = new Event(colorBlocked, commonUtil.getDateFromString(strDate).getTime(), "Blocked Date");
            compactCalendarView.addEvent(ev1, true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iconCal:
                if (compactCalendarView.getVisibility() == View.VISIBLE) {
                    compactCalendarView.setVisibility(View.GONE);
                } else {
                    compactCalendarView.setVisibility(View.VISIBLE);
                }
        }
    }
}
