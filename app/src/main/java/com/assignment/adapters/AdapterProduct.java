package com.assignment.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.assignment.R;
import com.assignment.entities.Product;
import com.assignment.interfaces.ListItemClickListener;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Saurabh on 02-08-2016.
 */
public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.MyHolder> {

    List<Product> products;
    LayoutInflater layoutInflater;
    ListItemClickListener listItemClickListener;
    Context context;

    public AdapterProduct(Context context, List<Product> products, ListItemClickListener listItemClickListener) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.products = products;
        this.listItemClickListener = listItemClickListener;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyHolder(layoutInflater.inflate(R.layout.item_product, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyHolder holder, final int position) {
        final Product product = products.get(position);
        holder.tvPrice.setText(context.getString(R.string.string_rupee) + product.getPerDayRent());
        holder.tvProductName.setText(product.getName());
        holder.tvRating.setText(product.getScore() + "");
        if (product.getPictures().size() > 0) {
            Picasso.with(context)
                    .load(product.getPictures().get(0).getUrl())
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.imgProduct);
        } else {
            holder.imgProduct.setImageResource(R.drawable.placeholder);
        }

        if (product.getOfferPercentage() == null || product.getOfferPercentage() == 0) {
            holder.tvOfferPct.setVisibility(View.GONE);
            holder.tvOfferPrice.setVisibility(View.GONE);
            holder.tvPrice.setPaintFlags(0);
        } else {
            holder.tvOfferPct.setVisibility(View.VISIBLE);
            holder.tvOfferPrice.setVisibility(View.VISIBLE);
            holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvOfferPct.setText(product.getOfferPercentage().intValue() + "% off");
        }
        if (product.getOfferPrice() == null || product.getOfferPrice() == 0) {
            holder.tvOfferPrice.setText("Not Avail.");
        } else {
            holder.tvOfferPrice.setText(context.getString(R.string.string_rupee) + product.getOfferPrice().toString());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.imgProduct.setTransitionName(context.getString(R.string.transition_name) + position);
        }
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listItemClickListener.itemClicked(position, product, holder);
            }
        });

    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        public TextView tvRating, tvProductName, tvPrice, tvOfferPrice, tvOfferPct;
        public ImageView imgProduct;
        public CardView cardView;

        public MyHolder(View itemView) {
            super(itemView);
            tvRating = (TextView) itemView.findViewById(R.id.tvRating);
            tvProductName = (TextView) itemView.findViewById(R.id.tvProductName);
            tvOfferPrice = (TextView) itemView.findViewById(R.id.tvOfferPrice);
            tvOfferPct = (TextView) itemView.findViewById(R.id.tvOfferPct);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            tvPrice = (TextView) itemView.findViewById(R.id.tvPrice);
            imgProduct = (ImageView) itemView.findViewById(R.id.imgProduct);
        }
    }
}
