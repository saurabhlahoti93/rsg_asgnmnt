package com.assignment.util;


public interface Constants {

    String DATE_FORMAT = "yyyy-MM-dd";
    String FRAGMENT_TAG = "PRODUCT";

    String TRANSITION_TAG = "imgTransition";
    String MY_SHARED_PREFERENCES = "application_data";
    String KEY_DEVICE_TOKEN = "deviceToken";
    String KEY_FACEBOOK = "authToken";
    String KEY_PRODUCTS = "products";

    enum SORT_MODE{
        NONE,
        NAME_ASC,
        NAME_DSC,
        PRICE_ASC,
        PRICE_DCS,
        RATE_ASC,
        RATE_DSC
    }

}
