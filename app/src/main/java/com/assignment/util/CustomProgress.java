package com.assignment.util;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.assignment.R;


/**
 * Created by saurabh on 11/02/16.
 */
public class CustomProgress {
    Context context;
    View view;
    String message;
    boolean cancellable;
    Dialog dialog;
    TextView tvMessage;
    ProgressBar progressBar;

    public CustomProgress(Context context, String message, boolean cancellable) {
        this.context = context;
        this.message = message;
        this.cancellable = cancellable;
        dialog = new Dialog(context);
        dialog.setCancelable(cancellable);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.progress_dialog);
        tvMessage = (TextView) dialog.findViewById(R.id.progressText);
        if (message != null) {
            tvMessage.setText(message);
        }

        progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar);
        progressBar.getProgress();
        progressBar.getIndeterminateDrawable().
                setColorFilter(context.getResources().getColor(R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    public void setCancellable(boolean cancellable) {
        dialog.setCancelable(cancellable);
    }

    public void setMessage(String message) {
        if (message != null) {
            tvMessage.setText(message);
        }

    }

    public void showProgress() {
        dialog.show();
    }

    public void dismissProgress() {
        dialog.dismiss();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setProgressBarDrawable(int rid) {
        progressBar.setIndeterminateDrawable(context.getDrawable(rid));
    }
}
