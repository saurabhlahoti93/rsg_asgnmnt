package com.assignment.util;

/**
 * Created by cl-macmini-80 on 20/06/16.
 */
public class Config {
    private static String BASE_URL = "";
    private static String GCM_KEY = "";

    static MODE mode = MODE.DEV;

    Config() {

    }

    public static String getBaseUrl() {
        init();
        return BASE_URL;
    }

    public static String getGcmKey() {
        return GCM_KEY;
    }

    static void init() {
        switch (mode) {
            case DEV:
                BASE_URL = "http://testapp.rentsetgo.co/";
                GCM_KEY = "";
                break;

            case LIVE:
                break;

        }
    }

    enum MODE {
        DEV, LIVE
    }
}
