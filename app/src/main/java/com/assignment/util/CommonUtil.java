package com.assignment.util;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.assignment.R;
import com.assignment.entities.Product;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;


//import org.apache.http.impl.DefaultHttpClientConnection;
//import org.apache.http.HttpRequest;

public class CommonUtil implements Constants {

    public static Context context;

    public Date getDateFromString (String str){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        try {
            date = simpleDateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public void sortNameAsc(List<Product> listItems) {
        Collections.sort(listItems, new Comparator<Product>() {
            // @Override
            public int compare(Product p1, Product p2) {
                return p1.getName().compareTo(p2.getName());
            }
        });
    }

    public void sortNamDsc(List<Product> listItems) {
        Collections.sort(listItems, new Comparator<Product>() {
            // @Override
            public int compare(Product p2, Product p1) {
                return p1.getName().compareTo(p2.getName());
            }
        });
    }

    public void sortRateAsc(List<Product> listItems) {
        Collections.sort(listItems, new Comparator<Product>() {
            // @Override
            public int compare(Product p1, Product p2) {
                return p1.getScore().compareTo(p2.getScore());
            }
        });
    }

    public void sortRateDsc(List<Product> listItems) {
        Collections.sort(listItems, new Comparator<Product>() {
            // @Override
            public int compare(Product p2, Product p1) {
                return p1.getScore().compareTo(p2.getScore());
            }
        });
    }

    public void sortPriceAsc(List<Product> listItems) {
        Collections.sort(listItems, new Comparator<Product>() {
            // @Override
            public int compare(Product p1, Product p2) {
                return p1.getPerDayRent().compareTo(p2.getPerDayRent());
            }
        });
    }

    public void sortPriceDsc(List<Product> listItems) {
        Collections.sort(listItems, new Comparator<Product>() {
            // @Override
            public int compare(Product p2, Product p1) {
                return p1.getPerDayRent().compareTo(p2.getPerDayRent());
            }
        });
    }



    public void showFacebookHashKey() {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getApplicationContext().getPackageName(), PackageManager.GET_SIGNATURES); //Your            package name here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                Log.e("PackageName:", context.getApplicationContext().getPackageName());
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }

    public void clearSharedPreferences() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.MY_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.KEY_FACEBOOK, null);
        editor.apply();
    }

    public String getStringFromSharedPreferences(String key, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.MY_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "");
    }

    public void setStringInSharedPreferences(String key, String value, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.MY_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }


    public void handleRetrofitError(Context context, Integer status, ResponseBody responseBody, Throwable throwable) {
        if (status == null) {
            String message = "";
            if (throwable.getMessage().contains("sockettimeout")) {
                message = context.getString(R.string.error_timeout);
            } else {
                message = context.getString(R.string.error_host);
            }
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

        } else {
            try {
                Toast.makeText(context, context.getString(R.string.error_some), Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}