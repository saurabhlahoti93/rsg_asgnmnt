package com.assignment;

import android.app.Application;
import android.content.Context;


import com.assignment.interfaces.WebServices;
import com.assignment.util.Config;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.UrlConnectionDownloader;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class GlobalClass extends Application {
    static GlobalClass globalClass;
    public WebServices webServices,googleServices;
    final String DOMAIN_GOOGLE_API = "https://maps.googleapis.com/maps/";
    @Override
    public void onCreate() {
        super.onCreate();
        try {
            Picasso.Builder builder = new Picasso.Builder(this);
            builder.downloader(new UrlConnectionDownloader(this));
            Picasso built = builder.build();
            built.setLoggingEnabled(true);
            Picasso.setSingletonInstance(built);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        globalClass = this;
    }

    public WebServices getInterface() {
        if (webServices == null) {
            OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClient.interceptors().add(interceptor);
            okHttpClient.readTimeout(15, TimeUnit.SECONDS);
            okHttpClient.connectTimeout(15, TimeUnit.SECONDS);
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(Config.getBaseUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient.build());
            Retrofit adapter = builder.build();
            webServices = adapter.create(WebServices.class);
        }
        return webServices;
    }
}
