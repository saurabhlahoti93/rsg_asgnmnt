### Rent Set Go - Assignment

#### External dependencies used
* [Facebook] -  Facebook Android SDK - For Facebook Login
* [Retrofit] - For making REST api calls
* [Picasso] - For ASYNC image loading
* [Compact Calendar View] - For Showing blocked dates
* RecyclerView & CareView dependencies
* OkHttp Logger - For Logging API Response
* GSON - For parsing json data to POJO
* Android Support Library


   [Facebook]: <https://github.com/facebook/facebook-android-sdk/>
   [Retrofit]: <https://github.com/square/retrofit/>
   [Compact Calendar View]  - <https://github.com/SundeepK/CompactCalendarView>
   [Picasso]: <https://github.com/square/picasso>
  